# @simperl/svelte-table

A simple svelte table component

[Demo](https://www.simperl.com/projects/svelte-table)

## Example

```
<script>

    import Table from '@simperl/svelte-table';

    import { onMount } from 'svelte';

    let rows = [];

    let columns = [
        { title : 'Name',       type : 'alpha', key : 'name' },
        { title : 'Date',       type : 'date',  key : 'date_local' },
        { title : 'Details',    type : 'alpha', key : 'details' },
    ];

    onMount(async () => {
        const res = await fetch(`https://api.spacexdata.com/v4/launches`);
        rows = await res.json();
    });

</script>


<Table  
    pagination_amounts = {[10,25,50,100]}
    sort_by = 'Date'
    columns = {columns}
    rows = {rows} />
```

## Usage

### Import 

Import @simperl/svelte-table as Table

```
import Table from '@simperl/svelte-table';
```

### columns
Define an array of columns in the order you want them to appear.

#### Column Options

##### title

This will be the name that appears in the column heading.

##### type
The type of the data used for the sorting
- alpha - for text sorting
- numeric - for numeric sorts
- date - sort by date * This is currently limited to dd/mm/YYYY, dd/mm/YYYY HH:MM:ss and YYYY-mm-ddTHH:MM:ss formats

##### key
This holds the path to the value to use as the key to sort by.

```
key : 'name'
```
This can be multi-level if your row data contains a customer object which has a name field you can access this.

```
key : 'customer.name'
```

##### template
If a template is defined this will be used to construct the contents of the cell. Placeholders can be defined with {} to place a given value from the row data as needed

e.g.
```
template : "<a target='_blank' href='{links.webcast}'>{name}</a>",
```

##### csv_template

This works the same as the template option but is used for the data in the csv. As it may not be appropriate to use the template which may include html in generating the csv.
```
csv_template : '{name}'
```

##### notMobile
Sets a notMobile class on the column so it can be easily removed with css for mobile devices.
```
notMobile : true
```

##### customClass
Adds any classes you want to a column to allow easy styling

```
customClass : 'myclass myotherclass'
```

### Rows

Your rows value should be an array of objects, this can be taken directly from an api or reprocessed before assigning to rows

### sort_by 
This defines the default column to sort by on load, it does this by referencing the column title.

```
sort_by : 'Name'
```

### pagination_amounts

Takes an array of values to paginate the data by.

```
pagination_amounts = {[10,25,50,100]}
```
